#include <iostream>

class Set
{
public:

	Set() :tab(), _size(0)
	{}

	unsigned int size() const
	{
		return _size;
	}

	bool contains(char c) const
	{
		return tab[c];
	}

	Set& insert(char c)
	{
		if(!contains(c))
		{
			tab[c] = true;
			++_size;
		}
		return *this;
	}

	Set& remove(char c)
	{
		if(contains(c))
		{
			tab[c] = false;
			--_size;
		}
		return *this;
	}

private:
	unsigned int _size;
	bool tab[127];
};

int main()
{
	Set set = Set().insert('a').insert('b').remove('b').remove('c');
	std::cout << set.size() << std::endl << std::boolalpha;
	std::cout << set.contains('a') << " " << set.contains('b') << " "
		<< set.contains('c') << " " << set.contains('d') << std::endl;
}
