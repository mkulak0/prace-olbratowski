#include<fstream>
#include<iostream>
#include<sstream>
#include<string>
#include<vector>

using namespace std;

int main(int argc, char *argv[]){
    ifstream file;
    file.open(argv[1]);

    //Wczytywanie pierwszej linii
    string first_line;
    char c;

    for(int i = 0; c != '\n' ; i++){
        file.get(c);
        first_line += c;
    }

    //Zapis pierwszej linii

    istringstream stream(first_line);

    vector<double> max_points;

    //Ten for to nawet nie jest potrzebny
    for(double temp; stream>>temp;){
        max_points.push_back(temp);
    }

    int numer_of_tasks = max_points.size();

    //Zapis linii ze studentami

    vector<string> student_lines;
    c = '\0';

    //Zmienna new_loop_process jest potrzebna żeby eof kiedyś zwrócił true
    bool new_loop_process;
    while(!file.eof()){
        string temp;
        new_loop_process = true;
        file.get(c);
        while(c != '\n'){
            if(!new_loop_process){
                file.get(c);
            }
            temp += c;
            new_loop_process = false;
        }
        if(temp != "" && temp != "\n"){

            student_lines.push_back(temp);

        }
    }

    int number_of_students = student_lines.size();

    //Inicjalizacja wektora z zerami
    vector<double> score_average;
    for(int i = 0; i < numer_of_tasks; i++){
        score_average.push_back(0);
    }

    //Wypisywanie studentów z wynikami i uzupełnianie tablicy z średnimi
    for(auto line: student_lines){
        istringstream stream(line);
        string name;
        stream>>name;
        cout<<name;

        double score = 0;
        int i = 0;
        for(double temp; stream>>temp;){
            score += temp;
            score_average[i] += temp;
            i++;
        }
        cout<<" "<<score<<endl;
    }

    //Wyświetlanie tablicy z średnimi
    int i = 1;
    for(auto line: score_average){
        cout<<i<<" "<<line/number_of_students<<endl;
        i++;
    }


    file.close();
}
