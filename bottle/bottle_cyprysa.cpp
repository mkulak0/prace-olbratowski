#include <iostream>

class Bottle
{
public:
	Bottle(double size): _size(size), _volume(0)
	{}
	const double& volume() const
	{
		return _volume;
	}
	double fill(double fill_bottle)
	{
		const double avaivable = _size - _volume;

		if(fill_bottle > avaivable)
		{
			_volume = _size;
			return avaivable;
		}

		_volume += fill_bottle;

		return fill_bottle;
	}

	double pour(double pour_volume)
	{
		if(pour_volume > _volume)
		{
			const double result = _volume;
			_volume = 0;
			return result;
		}

		_volume -= pour_volume;

		return pour_volume;
	}
private:
	double _size;
	double _volume;
};
int main()
{
	Bottle bottle(4.5);
	std::cout << bottle.volume() << " ";
	std::cout << bottle.fill(3.5) << " ";
	std::cout << bottle.volume() << " ";
	std::cout << bottle.pour(5.5) << " ";
	std::cout << bottle.volume() << std::endl;
}
