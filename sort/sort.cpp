#include<iostream>
#include<string>
#include<vector>

using namespace std;

int main(){
    vector<string> tab;
    string temp;
    while(cin>>temp){
        tab.push_back(temp);
    }

    bool changed;
    do{
        changed = false;
        for(int i = 0; i < (int)tab.size() - 1; i++){
            if(tab[i] > tab[i+1]){
                changed = true;
                string temp = tab[i];
                tab[i] = tab[i+1];
                tab[i+1] = temp;
            }
        }
    }while(changed);

    for(auto word: tab){
        cout<<word<<" ";
    }
}
