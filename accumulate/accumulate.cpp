#include<iostream>
#include<vector>

void accumulate(std::vector<double>::iterator first,  std::vector<double>::iterator last){
    double sum = 0;
    for(std::vector<double>::iterator i = first; i < last; i++){
        sum += *i;
        *i = sum;
    }
}

int main() {
std::vector<double> vector {3.1, 2.7, -0.5, 0.1, 4.3};
accumulate(vector.begin(), vector.end() - 2);
for (double element: vector) {
std::cout << element << " "; }
std::cout << std::endl; }
