#include<iostream>
#include<string>
#include<fstream>
#include<vector>

std::vector<int> count(std::string file_name, std::string looking_for_string){

    std::fstream file;
    file.open(file_name);

    std::vector<int> result;

    if(file.good()){

        std::string line = "";
        for(int line_num = 0 ;!file.eof(); line_num++){
            std::getline(file, line);

            bool found_previously = false;
            result.push_back(0);

            int char_num_looking = 0;
            for(int char_num_input = 0; char_num_input < line.length(); char_num_input++){

                if(line[char_num_input] == looking_for_string[char_num_looking]){
                    if(char_num_looking == looking_for_string.length() - 1){

                        char_num_looking = 0;
                        result[line_num]++;
                    }
                    else {
                        char_num_looking++;
                    }
                }

            }
        }

    }
    return result;

}

int main(){
    std::vector<int> vector = count(std::string("input.txt"), std::string("al"));
    for(int element: vector) {
        std::cout << element << " ";
    }
    std::cout<<std::endl;
}
