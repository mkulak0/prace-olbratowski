#include<iostream>
#include<fstream>
#include<vector>


void show(int num){
    int hour = num/100;
    int minutes = num%100;
    if(minutes < 10){
        std::cout<<hour<<":0"<<minutes<<std::endl;
    } else {
        std::cout<<hour<<":"<<minutes<<std::endl;
    }
}

int main(int argc, char *argv[]){
    std::fstream file;
    file.open(argv[1]);
    std::vector<size_t> tab;

    int hour;
    char colon;
    int minutes;

    while(!file.eof()){
        file>>hour>>colon>>minutes;
        tab.push_back(100*hour + minutes);
    }

    while(std::cin>>hour>>colon>>minutes){
        int num = 100 * hour + minutes;
        bool outputted = false;
        for(int i = 0; i < tab.size(); i++){
            if(num <= tab[i]){
                //std::cout<<tab[i]<<std::endl;
                show(tab[i]);
                outputted = true;
                break;
            }
        }
        if(!outputted){
            show(tab[0]);
        }

    }

    file.close();
}
