#include<vector>
#include<iostream>

std::vector<int>::const_iterator find(std::vector<int>::const_iterator first, std::vector<int>::const_iterator last, int number){
    for(std::vector<int>::const_iterator i = first; i < last; i++){
        if(*i == number){
            return i;
        }
    }
    return last;
}

int main() {
    const std::vector<int> vector {3, -1, 7, 12, -5, 7, 10};
    std::cout << find(vector.begin() + 3, vector.end(), 7) - vector.begin() << std::endl;
}