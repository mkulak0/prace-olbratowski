### Zasady korzystania z repozytorium

* **Nie wszystko może działać. To tylko wolna interpretacja.**
* Wysyłasz pracę = zmień chociaż nazwy zmiennych
* Sprawdzarka czasem usuwa *using name std!*
* **Maciej K.** odpowiada za rozprowadzanie linków. Chcesz komuś udostępnić pracę = zapytaj. *(Za każdym udostępnieniem nieznanemu IP dostaje maila, więc bądźmy fair)*
* Chcesz dodać swój kod? Zarejestruj się na bitbucket a dam ci uprawnienia. 

---

### Do zrobienia:
#### Operatory
* Add
* Geometric
#### Metody
* Histogram
* Object
#### Dziedziczenie
* Brak danych :(
#### Kolokwium
* K11
* K12
* K13
* K14
