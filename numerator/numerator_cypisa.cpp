#include<fstream>

int main(int argc, char **argv)
{
	const char *file_input = argv[1];
   	std::ifstream input(file_input);

	const char *file_output= argv[2];
	std::ofstream output(file_output);
	
	if(input.peek() == -1){
        return 1;
    }

	size_t line = 1;
	char c;
	output << line++ << ' ';
	while(input.get(c))
	{
		output << c;
		if(c == '\n' && input.peek() != -1) output << line++ << ' ';
	}
} 
