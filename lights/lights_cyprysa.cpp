#include <iostream>

class Lights
{
public:
	Lights() : lightStates{true, false, false}, _cycle_number()
	{}

	bool red() const
	{
		return lightStates[LightType::red_t];
	}
	bool yellow() const
	{
		return lightStates[LightType::yellow_t];
	}
	bool green() const
	{
		return lightStates[LightType::green_t];
	}
	void flip()
	{
		if(++_cycle_number > 3)
			_cycle_number = 0;

		switch(_cycle_number)
		{
			case 0:
			{
				lightStates[LightType::red_t] = true;
				lightStates[LightType::yellow_t] = false;
				break;
			}
			case 1:
			{
				lightStates[LightType::yellow_t] = true;
				break;
			}
			case 2:
			{
				lightStates[LightType::red_t] = false;
				lightStates[LightType::yellow_t] = false;
				lightStates[LightType::green_t] = true;
				break;
			}
			case 3:
			{
				lightStates[LightType::yellow_t] = true;
				lightStates[LightType::green_t] = false;
				break;
			}
		}
	}

private:
	enum LightType
	{
		red_t,
		yellow_t,
		green_t
	};

	bool lightStates[3];
	unsigned char _cycle_number;
};

int main()
{
	std::cout << std::boolalpha;
	Lights lights;
	std::cout << lights.red() << " " << lights.yellow() << " " << lights.green() << std::endl;
	lights.flip();
	std::cout << lights.red() << " " << lights.yellow() << " " << lights.green() << std::endl;
	lights.flip();
	std::cout << lights.red() << " " << lights.yellow() << " " << lights.green() << std::endl;
	lights.flip();
	std::cout << lights.red() << " " << lights.yellow() << " " << lights.green() << std::endl;
	lights.flip();
	std::cout << lights.red() << " " << lights.yellow() << " " << lights.green() << std::endl;
}
